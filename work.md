---
title: Landscaping
image: pic04.jpg
---
Turning ecologically sterile grass and non-natives into something good for bees, birds, butterflies, and the soil.     
    
<a href="https://rabbitrabbitwhiterabbit.tumblr.com/post/622197351272087552/showalter-road">Click to see</a> the process for a simple low-to-no-maintenance addition to a yard without power tools or expensive landscaping elements.     
