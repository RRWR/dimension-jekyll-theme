---
title: Consulting
image: compostable.jpg
---
Rabbit Rabbit White Rabbit is committed to helping change the way people interact with their environment. 

Our sustainability consulting focuses on making small businesses - especially restaurants - more efficient, economical, and more mindful of the environment.  
  
They do this by earning the Green Rabbit Seal of Approval (GRSA). Like LEED or VEEP certification, but for small business operations, the GRSA assesses firms based on their energy efficiency, input sourcing, wastestream management, carbon footprint, and their ongoing relationship with the environment and their community, among other things.  
  
Businesses are rated based on several metrics that apply specifically to their operation, and then RRWR helps them develop an individualized, ongoing plan to meet agreed upon sustainability targets, whether that means cutting down on single-use plastic, finding a new product supplier, or installing solar panels.  
  
The ratings are relative, meaning the more you can do from where you are, the better your score.  
  
Those that reach a threshhold for commitment to sustainability earn the GRSA and are added to our network of local businesses, which lets people know where to spend their time and money responsibly.  
  
The network is growing. Contact us today to schedule a consultation and become a Green Rabbit.

